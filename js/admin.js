(function ($) {
	$.ajaxSetup({
		cache: false
	});
	$('body').on('keyup', '.deliveo-modal', function (e) {
		if (e.key === "Escape") {
			$(this).remove();
		}
	})
	$(document).on('keydown', function (event) {
		if (event.key == "Escape") {
			$('body').css('overflow', 'auto');
			$('.deliveo-modal').remove();
		}
	});
	$(document).on('click', '#deliveo-save-api-licence-1', function () {
		var apiKey = $('#deliveo-api-key').val();
		var licenceKey = $('#deliveo-licence-key').val();

		console.log(licenceKey)
		if (apiKey.length > 0 && licenceKey.length > 0) {
			$.ajax({
				type: 'post',
				url: ajaxurl,
				dataType: 'json',
				data: {
					action: 'save_api_key_and_licence',
					licence_key: $('#deliveo-licence-key').val(),
					api_key: $('#deliveo-api-key').val(),
					_nonce: jQuery('input[name="_nonce"]').val()
				},
				success: function (response) {
					location.reload();
				}
			});
		}
	});



	$(document).on('click', '.deliveo-cell', function () {
		var deliveo_id = $(this).data('groupid');
		label_download(deliveo_id)

	});
	$(document).on('click', '#deliveo-close', function () {
		$('.deliveo-modal').remove();
		$('body').css('overflow', 'auto');
	})

	function packageLog(deliveo_id) {
		$.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				action: 'view_package_log',
				deliveo_id: deliveo_id
			},
			success: function (response) {
				$('#package_log').html(response);
			},
			error: function (errorThrown) {
				console.warn(errorThrown);
			}
		});
	}

	function view_signature(deliveo_id) {
		$.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				action: 'view_signature',
				deliveo_id: deliveo_id
			},
			success: function (response) {
				var resp = JSON.parse(response);
				if (resp['img'] != '') {
					$('#deliveo-signature').html(resp['img']);
				} else {
					$('#deliveo-signature').remove();
				}

				packageLog(deliveo_id);
			},
			error: function (errorThrown) {
				console.warn(errorThrown);
			}
		});
	}

	function label_download(deliveo_id) {
		$.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				action: 'label_download',
				deliveo_id: deliveo_id
			},
			success: function (response) {
				$('body').css('overflow', 'hidden');
				var modal = '<div class="deliveo-modal container">';
				modal += '<div class="content">';
				modal += '<h2><span></span>' + deliveo_id + '</h2>';
				modal += '<hr>';
				modal += '<div id="package_info"></div>';
				modal += '<div id="package_log" style="text-align:center;">Loading...</div>';
				modal += '<hr>';
				modal += '<div id="deliveo-footer">';
				modal += '<div id="deliveo-label">' + response + '</div>';
				modal += '<div id="deliveo-signature"></div>';
				modal += '<div id="deliveo-close">×</div>';
				modal += '</div>';
				modal += '</div>';
				modal += '</div>';
				$('body').append(modal);
				$('.deliveo-modal').addClass('show');
				packageView(deliveo_id);
				view_signature(deliveo_id);
			},
			error: function (errorThrown) {
				console.warn(errorThrown);
			}
		});
	}

	function packageView(deliveo_id) {
		$.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				action: 'package_details',
				deliveo_id: deliveo_id
			},
			success: function (response) {
				$('#package_info').html(response);
			},
			error: function (errorThrown) {
				console.warn(errorThrown);
			}
		});
	}
})(jQuery)
