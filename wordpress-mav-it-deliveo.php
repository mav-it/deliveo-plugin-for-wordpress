<?php

/**
 * Plugin Name: Deliveo
 * Plugin URI: https://wordpress.org/plugins/deliveo/
 * Description: Deliveo plugin for WordPress
 * Author: Deliveo
 * Author URI: https://deliveo.eu
 * Developer: Deliveo
 * Developer URI: https://deliveo.eu
 * Text Domain: deliveo
 * Version: 2.4.8
 * Domain Path: /languages
 *
 * License: GNU General Public License v3.0
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 *
 * WC requires at least: 6.0.0
 * WC tested up to: 8.2
 */

use Automattic\WooCommerce\Utilities\OrderUtil;

include_once __DIR__ . '/inc/helpers.php';
include_once __DIR__ . '/inc/deliveo-request-filter.class.php';
include_once __DIR__ . '/inc/mav-it-deliveo.class.php';
include_once __DIR__ . '/inc/csv-export.class.php';
include_once __DIR__ . '/inc/deliveo-api.class.php';
include_once __DIR__ . '/inc/deliveo-settings.class.php';
define('MAV_IT_DELIVEO_DIR_URL', plugin_dir_url(__FILE__));

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

add_action('init', 'setTranslate');
add_action('wp_login',  'updateShippingOptions');
add_action('admin_notices',  'deliveo_success_msg');


add_filter('bulk_actions-woocommerce_page_wc-orders', 'add_bulk_actions', 10, 1);
add_filter('bulk_actions-edit-shop_order', 'add_bulk_actions', 10, 1);

add_action('bulk_actions',  'custom_bulk_action');
//add column content
add_filter('manage_shop_order_posts_custom_column', 'add_order_packaging_column_content', 10, 2);
add_filter('manage_woocommerce_page_wc-orders_custom_column', 'add_order_packaging_column_content', 10, 2);

add_filter('handle_bulk_actions-woocommerce_page_wc-orders', 'handleDeliveoBulkAction', 10, 3);
add_filter('handle_bulk_actions-edit-shop_order', 'handleDeliveoBulkAction', 10, 3);
add_filter('manage_edit-shop_order_columns',  'add_deliveo_columns_header', 20);
add_action('wp_ajax_label_download', 'label_download');

add_action('wp_ajax_view_signature',  'view_signature');
add_action('wp_ajax_view_package_log', 'view_package_log');
add_action('wp_ajax_package_details', 'package_details');




add_action('admin_menu', 'mav_it_deliveo_add_admin_menu', 99);

//add javascript files to admin 
add_action('admin_enqueue_scripts', function () {
    wp_enqueue_script('deliveo-admin-js', MAV_IT_DELIVEO_DIR_URL . 'js/admin.js', array('jquery'), '1.0.0', true);
    wp_enqueue_style('deliveo-admin-css', MAV_IT_DELIVEO_DIR_URL . 'css/mav-it-deliveo.css', array(), '1.0.0', 'all');
});

function mav_it_deliveo_add_admin_menu()
{
    add_submenu_page('woocommerce', __('Deliveo', 'mav-it'), __('Deliveo', 'mav-it'), 'manage_options', 'mav_it_deliveo', ['\MAV_IT_Deliveo_Settings', 'initMenuItem']);
}

add_filter('woocommerce_shop_order_list_table_columns', function ($columns) {

    // move the column to the fifth position
    $columns = array_slice($columns, 0, 4, true) +
        array('group_code' => 'DeliveoID') +
        array_slice($columns, 4, count($columns) - 1, true);
    $columns = array_slice($columns, 0, 5, true) +
        array('packaging_unit' => 'Colli') +
        array_slice($columns, 5, count($columns) - 1, true);
    //add shipping method to order list
    $columns = array_slice($columns, 0, 3, true) + array('shipping_method' => 'Szállítási mód') + array_slice($columns, 3, count($columns) - 1, true);


    return $columns;
}, 1);

function handleDeliveoBulkAction($redirect_to, $action, $post_ids)
{

    if ($action !== 'deliveo_api') {
        return $redirect_to;
    }

    $succeded = 0;
    $failed = 0;
    $failed_message = [];
    $deliveo_settings_obj = new MAV_IT_Deliveo_Settings();
    $mav_it_deliveo = new MAV_IT_Deliveo($deliveo_settings_obj);

    foreach ($post_ids as $post_id) {
        $send = $mav_it_deliveo->send_by_api((int)$post_id, getQueryShippingOption($post_id), getQueryUnit($post_id));
        if ($send->type == "success") {
            $succeded++;
        }
        if ($send->type == "warning") {
            $failed++;
            $failed_message[] = 'OrderID: ' . $post_id . ' - ' . $send->msg . ": " . ($send->field ?? '');
        }
        if ($send->type == "error") {
            $failed++;
            if ($send->message) {
                $failed_message[] = 'OrderID: ' . $post_id . ' - ' . ($send->message) . ": " . ($send->field ?? '');
            } elseif ($send->msg) {
                $failed_message[] = 'OrderID: ' . $post_id . ' - ' . ($send->msg);
            }
        }
    }

    $feedback = array(
        'deliveo_ok' => true,
        'succeded' => $succeded,
        'failed' => $failed,
        'failed_messages' => implode('<br/>', $failed_message)
    );

    $feedback["deliveo_ok"] = $failed == 0;

    if (count($failed_message) > 0) {
        $feedback['failed_messages'] = implode('<br/>', $failed_message);
    }


    $sendback = add_query_arg($feedback, wp_get_referer());

    return $sendback;
}

add_filter('post_class', function ($classes) {
    $classes[] = 'no-link';
    return $classes;
});

add_filter('woocommerce_shop_order_list_table_columns', function ($columns) {
    // move the column to the fifth position
    $columns = array_slice($columns, 0, 4, true) +
        array('group_code' => 'DeliveoID') +
        array_slice($columns, 4, count($columns) - 1, true);
    $columns = array_slice($columns, 0, 5, true) +
        array('packaging_unit' => 'Colli') +
        array_slice($columns, 5, count($columns) - 1, true);

    return $columns;
}, 1);

add_action('before_woocommerce_init', function () {
    if (class_exists(\Automattic\WooCommerce\Utilities\FeaturesUtil::class)) {
        \Automattic\WooCommerce\Utilities\FeaturesUtil::declare_compatibility('custom_order_tables', __FILE__, true);
    }
});

add_action('woocommerce_order_status_changed', 'auto_send_to_deliveo', 10, 3);
function setTranslate()
{
    $userId = get_current_user_id();
    load_textdomain('mav-it', WP_PLUGIN_DIR . '/' . dirname(plugin_basename(__FILE__)) . '/languages/' . get_user_locale($userId) . '.mo');
    //add bulk actions to orders list
}
add_deliveo();

function add_deliveo() {}

function add_bulk_actions($bulk_actions)
{
    $bulk_actions['deliveo_api'] = __('Deliveo API feladás', 'mav-it');

    return $bulk_actions;
}



function auto_send_to_deliveo($order_id, $old_status, $new_status)
{

    error_log('auto_send_to_deliveo ' . $order_id . ' ' . $old_status . ' ' . $new_status);

    $succeded = 0;
    $failed = 0;
    $failed_message = [];

    $deliveo_settings_obj = new MAV_IT_Deliveo_Settings();

    $mav_it_deliveo = new MAV_IT_Deliveo($deliveo_settings_obj);
    $group_code = get_metadata('post', $order_id, '_group_code', true);
    if (
        is_null($mav_it_deliveo->deliveo_settings['send_trigger']) ||
        $mav_it_deliveo->deliveo_settings['send_trigger'] == "" ||
        strlen($group_code) > 0 ||
        $mav_it_deliveo->deliveo_settings['send_trigger'] == $mav_it_deliveo->deliveo_settings['delivered_status'] ||
        ($mav_it_deliveo->deliveo_settings['send_trigger'] != 'wc-' . $new_status)
    ) {
        return;
    }

    $send = $mav_it_deliveo->send_by_api((int)$order_id, getQueryShippingOption($order_id), getQueryUnit($order_id));
    if ($send->type == "success") {
        $succeded++;
    }

    if ($send->type == "warning") {
        $failed++;
        $failed_message[] = 'ID: ' . $order_id . ' - ' . $send->msg;
    }

    $feedback = array(
        'deliveo_ok' => true,
        'succeded' => $succeded,
        'failed' => $failed,
        'failed_messages' => implode('<br/>', $failed_message)
    );

    foreach ($feedback as $key => $row) {
        add_query_arg($key, $row);
    }
}

function getQueryShippingOption($orderID)
{
    if (isset($_GET['order']))
        foreach ((array)$_GET['order'] as $key => $data) {
            if ($key == $orderID) {
                return $data['option'];
            }
        }


    return false;
}
function getQueryUnit($orderID)
{
    if (isset($_GET['order']))
        foreach ((array)$_GET['order'] as $key => $data) {
            if ($key == $orderID) {
                return $data['unit'];
            }
        }

    return false;
}

function updateShippingOptions()
{
    $deliveo_api = new Deliveo_API(new MAV_IT_Deliveo_Settings());
    $options = new MAV_IT_Deliveo_Settings();
    $shipping_options = $deliveo_api->get_shipping_options();

    $opts = [];
    if ($shipping_options) {
        foreach ($shipping_options as $opt) {
            $opts[] = [
                'value' => $opt->value,
                'description' => $opt->description,
                'alias' => $opt->alias,
                'shipping_default' => $opt->shipping_default,
            ];
        }
        $options->deliveo_settings['shipping_options'] = $opts;

        $settings = $options;
        update_option('mav_it_deliveo_settings', json_encode($settings->deliveo_settings));
    }
}
function add_deliveo_columns_header($columns)
{
    $new_columns = array();
    $index = 0;

    foreach ($columns as $column_name => $column_info) {
        $new_columns[$column_name] = $column_info;

        if ($index == 1) {
            $new_columns['group_code'] = '<img style="vertical-align:middle" src="' . MAV_IT_DELIVEO_DIR_URL . 'images/deliveo-icon.png" alt="DeliveoID" title="' . __('Amelyik rendelésnél ilyen ikont lát, az már fel lett adva Deliveo API-n keresztül.', 'mav-it') . '" /> DeliveoID';

            if (json_decode(get_option('mav_it_deliveo_settings'))->packaging_unit == 2) {
                $new_columns['packaging_unit'] = 'Colli';
            }
        }

        $index += 1;
    }

    return $new_columns;
}

function custom_bulk_action()
{
    $wp_list_table = _get_list_table('WP_Posts_List_Table');
    $action = $wp_list_table->current_action();

    switch ($action) {
        case 'deliveo_api':

            $deliveo = new MAV_IT_Deliveo(new MAV_IT_Deliveo_Settings());
            $succeded = 0;
            $failed = 0;
            $failed_message = [];

            $orderArray = $_GET['order'] ?? [];
            $postArray = $_GET['post'] ?? [];

            foreach ($orderArray as $order) {
                if (in_array($order['id'], $postArray)) {
                    $send = $deliveo->send_by_api((int)$order['id'], $order['option'], $order['unit']);

                    if ($send->type == "success") {
                        $succeded++;
                    }
                    if ($send->type == "warning") {
                        $failed++;
                        $failed_message[] = 'OrderID: ' . $order['id'] . ' - ' . $send->msg . ": " . ($send->field ?? '');
                    }
                }
            }
            $feedback = array(
                'deliveo_ok' => true,
                'succeded' => $succeded,
                'failed' => $failed,
                'failed_messages' => implode('<br/>', $failed_message)

            );

            $sendback = add_query_arg($feedback, wp_get_referer());

            break;
        case 'deliveo_export':
            $deliveo = new MAV_IT_Deliveo(new MAV_IT_Deliveo_Settings());
            $ids = [];
            $orderArray = Deliveo_Request_Filter::getInstance()
                ->getOrderArray(INPUT_GET, 'order');
            if (count($orderArray) > 0) {

                foreach ($orderArray as $order) {
                    $ids[] = $order['id'];
                }
                $deliveo->generate_csv($ids);
            } else {
            }
            $sendback = add_query_arg(array(
                'deliveo_export_failed' => true
            ), wp_get_referer());
            // $sendback = add_query_arg(array('deliveo_ok' => true, 'succeded' => $succeded, 'failed' => $failed), wp_get_referer());

            break;
        default:
            return;
    }

    wp_redirect($sendback);

    exit();
}

function add_order_packaging_column_content($column, $post_id)
{

    $order = new WC_Order($post_id);

    if ($column == 'packaging_unit') {
        $exported = get_metadata('post', $order->get_id(), '_deliveo_exported', true);

        $packaking_unit = json_decode(get_option('mav_it_deliveo_settings'))->packaging_unit;
        if ($exported != 'true') {
            switch ((int)$packaking_unit) {
                case 0:
                    echo "<input title='Colli' name='order[" . $order->get_id() . "][unit]' type='number' readonly value='1' class='no-link'>";
                    break;
                case 1:
                    echo "<input title='Colli' name='order[" . $order->get_id() . "][unit]' type='number' readonly value='" . $order->get_item_count() . "' class='no-link'>";
                    break;
                case 2:
                    echo "<input title='Colli' name='order[" . $order->get_id() . "][unit]' type='number' value='" . $order->get_item_count() . "' max='" . $order->get_item_count() . "' min='1' class='no-link'>";
                    break;
                default:
                    echo "<input title='Colli' type='number' value='" . $order->get_item_count() . "' max='" . $order->get_item_count() . "' min='1' class='no-link'>";
                    break;
            }
        }
    }
    if ('group_code' === $column) {

        $exported = get_metadata('post', $order->get_id(), '_deliveo_exported', true);
        $group_code = get_metadata('post', $order->get_id(), '_group_code', true);

        if ($exported == 'true') {
            echo ('<span class="no-link deliveo-cell" style="cursor:pointer;font-weight:600;color:#0073aa;" data-groupid="' . $group_code . '"> ' . $group_code . '</span>');
        } else {
            $deliveo_api = new Deliveo_API(new MAV_IT_Deliveo_Settings());
            $selected = '';
            $opts = json_decode(get_option('mav_it_deliveo_settings'))->shipping_options;
            echo "<input type='hidden' name='order[" . $order->get_id() . "][id]' value='" . $order->get_id() . "'>";
            echo '<select class="no-link" data-id="' . $order->get_id() . '" style="max-width:100%" name="order[' . $order->get_id() . '][option]" title="' . __('Válasszon szállítási módot', 'mav-it') . '">';
            foreach ($opts as $option) {
                if ($deliveo_api->api_settings_obj->deliveo_settings["delivery"] == $option->value) {
                    $selected = 'selected';
                } else {
                    $selected = '';
                }
                echo '<option  value="' . $option->value . '" ' . $selected . '>' . $option->description . '</option>';
            }
            echo '</select>';
        }
    }

    if ('shipping_method' === $column) {
        $shipping_method = $order->get_shipping_method();
        echo $shipping_method;
    }
}


function deliveo_success_msg()
{

    $deliveoOk = Deliveo_Request_Filter::getInstance()
        ->getFilteredVar(INPUT_GET, 'deliveo_ok');

    $succeded = Deliveo_Request_Filter::getInstance()
        ->getFilteredVar(INPUT_GET, 'succeded');

    $failed = Deliveo_Request_Filter::getInstance()
        ->getFilteredVar(INPUT_GET, 'failed');
    $failedMessages = Deliveo_Request_Filter::getInstance()
        ->getFilteredVar(INPUT_GET, 'failed_messages');


    $deliveoExportFailed = Deliveo_Request_Filter::getInstance()
        ->getFilteredVar(INPUT_GET, 'deliveo_export_failed');
    if ($deliveoOk) {
?>
        <div class="notice notice-success is-dismissible">
            <p><strong>
                    <h2><?php echo __('Deliveo csomagfeladás befejezve', 'mav-it') ?></h2>
                </strong><?php echo __('Sikeres', 'mav-it') . ': ' . $succeded . ', ' . __('sikertelen') . ': ' . $failed ?></p>
        </div>'
<?php
    }
    if ($failedMessages) {
        foreach ((array)$failedMessages as $msg) {
            echo '<div class="notice notice-error">
                <p>' . $msg . '</p>
                </div>';
        }
    }

    if ($deliveoExportFailed) {
        echo '<div class="notice notice-error">
                 <p><strong><h2>' . __('Deliveo Export hiba', 'mav-it') . '</h2></strong>' . __('Nem sikerült exportálni a rendeléseket. A korábban feladott rendeléseket már nem tölthetjük le!', 'mav-it') . '</p>
             </div>';
    }
}

function label_download()
{
    $groupId = Deliveo_Request_Filter::getInstance()
        ->filterBasicCode(INPUT_POST, 'deliveo_id');

    $deliveo_settings_obj = new MAV_IT_Deliveo_Settings();
    $mav_it_deliveo = new MAV_IT_Deliveo($deliveo_settings_obj);
    $settings    = $mav_it_deliveo->deliveo_settings;
    $label_url   = "https://api.deliveo.eu/label/" . $groupId . "?licence=" . $settings["licence_key"] . "&api_key=" . $settings["api_key"];
    $package_url = "https://api.deliveo.eu/package/" . $groupId . "?licence=" . $settings["licence_key"] . "&api_key=" . $settings["api_key"];

    $tmpfile = download_url($label_url, $timeout = 300);

    $check_signature = json_decode(wp_remote_fopen($package_url), true);
    if ($check_signature['data'][0]['deliveo_id'] == $groupId) {
        $permfile = $groupId . '.pdf';
        $destfile = wp_get_upload_dir()['path'] . "/" . $groupId . '.pdf';
        $dest_url = wp_get_upload_dir()['url'] . "/" . $groupId . '.pdf';
        copy($tmpfile, $destfile);
        unlink($tmpfile);
        $package =
            '<a target="blank" href="' . $dest_url . '"><img title="' . __('Csomagcímke letöltése', 'mav-it') . '"  style="vertical-align:middle;height:36px;" src="' . MAV_IT_DELIVEO_DIR_URL . 'images/deliveo-package-barcode.png" ></a>';
        echo $package;
    } else {
        $package =
            '<img title="' . __('Nincs csomagcímke', 'mav-it') . '"  style="vertical-align:middle;height:36px;filter:grayscale(100%);" src="' . MAV_IT_DELIVEO_DIR_URL . 'images/deliveo-package-barcode.png" >';
        echo $package;
    }
    wp_die();
}

function view_signature()
{
    $groupId = Deliveo_Request_Filter::getInstance()
        ->filterBasicCode(INPUT_POST, 'deliveo_id');
    $deliveo_settings_obj = new MAV_IT_Deliveo_Settings();
    $mav_it_deliveo = new MAV_IT_Deliveo($deliveo_settings_obj);

    $settings      = $mav_it_deliveo->deliveo_settings;
    $signature_url = "https://api.deliveo.eu/signature/" . $groupId . "?licence=" . $settings["licence_key"] . "&api_key=" . $settings["api_key"];

    $tmpfile = download_url($signature_url, $timeout = 300);

    $check_signature = json_decode(wp_remote_fopen($signature_url), true);
    if ($check_signature['type'] == 'error') {
        $response = array(
            "error" => "no_signature",
            "img"   => '<img title="' . __('Nincs aláíráslap', 'mav-it') . '"  style="vertical-align:middle;height:24px; filter: grayscale(100%);" src="' . MAV_IT_DELIVEO_DIR_URL . 'images/deliveo-signature.png" ></a>',
        );
        echo json_encode($response);
    } else {
        $permfile = $groupId . '_sign.pdf';
        $destfile = wp_get_upload_dir()['path'] . "/" . $groupId . '_sign.pdf';
        $dest_url = wp_get_upload_dir()['url'] . "/" . $groupId . '_sign.pdf';
        copy($tmpfile, $destfile);
        unlink($tmpfile);
        $package = array(
            'url' => $dest_url,
            'img' => '<a href="' . $dest_url . '" target="blank"><img title="' . __('Aláíráslap letöltése', 'mav-it') . ': ' . $groupId . '"  style="vertical-align:middle;height:24px;" src="' . MAV_IT_DELIVEO_DIR_URL . 'images/deliveo-signature.png" ></a>',
        );
        echo json_encode($package);
    }
    wp_die();
}

function view_package_log()
{
    $groupId = Deliveo_Request_Filter::getInstance()
        ->filterBasicCode(INPUT_POST, 'deliveo_id');
    $deliveo_settings_obj = new MAV_IT_Deliveo_Settings();
    $mav_it_deliveo = new MAV_IT_Deliveo($deliveo_settings_obj);

    $settings        = $mav_it_deliveo->deliveo_settings;
    $package_log_url = "https://api.deliveo.eu/package_log/" . $groupId . "?licence=" . $settings["licence_key"] . "&api_key=" . $settings["api_key"];

    $package_log = json_decode(wp_remote_fopen($package_log_url), true)['data'];

    if (isset($package_log[0])) {
        $row = '<h4>' . __('Napló', 'mav-it') . '</h4>';
    } else {
        $row = '<h4 style="background-color:red">' . __('Napló nem található', 'mav-it') . '</h4>';
    }

    foreach ($package_log as $entry) {
        $status =
            $row .= '<div align="left" class="row">';
        $row .= date('Y-m-d H:i', $entry['timestamp']) . '<br>';
        $row .= $mav_it_deliveo->displayStatus($entry['status'], $entry['status_text']);
        $row .= '</div>';
        // unset($row);
        $row .= '</div>';
    }
    echo $row;

    wp_die();
}

function package_details()
{
    $groupId = Deliveo_Request_Filter::getInstance()
        ->filterBasicCode(INPUT_POST, 'deliveo_id');

    $deliveo_settings_obj = new MAV_IT_Deliveo_Settings();
    $mav_it_deliveo = new MAV_IT_Deliveo($deliveo_settings_obj);


    $settings        = $mav_it_deliveo->deliveo_settings;
    $package_log_url = "https://api.deliveo.eu/package/" . $groupId . "?licence=" . $settings["licence_key"] . "&api_key=" . $settings["api_key"];
    // $signature_url = "https://api.deliveo.eu/signature/" . $groupId . "?licence=" . $settings["licence_key"] . "&api_key=" . $settings["api_key"];

    $package_details = json_decode(wp_remote_fopen($package_log_url), true)['data'];
    if ($package_details[0]['dropped_off'] != null) {
        $delivered = __('Átvette', 'mav-it') . ' (' . $package_details[0]['dropped_off'] . ')';
    } else {
        $delivered = __('A küldemény még nincs kézbesítve.', 'mav-it');
    }
    if (isset($package_details[0])) {

        echo '<div class="content">' . $delivered . '<br></div>';
        echo '<div class="group_info" style="background-image:url(' . MAV_IT_DELIVEO_DIR_URL . 'images/deliveo-icon.png' . ')">';

        echo '<h4>' . __('Címzett', 'mav-it') . '</h4>';
        echo '<div class="content">[' . $package_details[0]['consignee_zip'] . '] ' . $package_details[0]['consignee_city'] . '</div>';
        echo '<div class="content">' . $package_details[0]['consignee_address'] . ' ' . $package_details[0]['consignee_apartment'] . '</div>';
        echo '<div class="content">' . $package_details[0]['consignee_phone'] . ' | <a href="mailto:' . $package_details[0]['consignee_email'] . '">' . $package_details[0]['consignee_email'] . '</a></div>';

        echo '</div>';
        echo '</div>';
    } else {
        echo '<div class="group_info" style="background-image:url(' . MAV_IT_DELIVEO_DIR_URL . 'images/deliveo-icon.png' . ')">';
        echo '<h4>' . __('A küldemény nem található.', 'mav-it') . '</h4>';
        echo '</div>';
    }
    wp_die();
}
