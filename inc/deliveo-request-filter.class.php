<?php

class Deliveo_Request_Filter
{
    private static $INSTANCE = null;
    private $postTypes = null;

    /**
     * @return Deliveo_Request_Filter|null
     */
    public static function getInstance()
    {
        if (self::$INSTANCE === null) {
            self::$INSTANCE = new self();
        }
        return self::$INSTANCE;
    }

    private function __construct()
    {
        $this->postTypes = get_post_types();
    }

    /**
     * @param $source
     * @param $key
     * @param int $filter
     * @param int $options
     * @return mixed|null
     */
    private function getValueFromGlobalArray($source, $key, $filter = FILTER_DEFAULT, $options = [])
    {
        $value = filter_input($source, $key, $filter, $options);

        if ($value === false) return null;
        return $value;
    }

    public function getFilteredVar($source, $key)
    {
        $value = filter_input($source, $key);
        if ($value === null || $value === false) return null;
        return $value;
    }

    /**
     * @param int $source
     * @param string $key
     * @param null|string $default
     * @return string|null
     */
    public function getPostType($source, $key, $default = null)
    {
        $value = $this->getValueFromGlobalArray($source, $key);
        if ($value === null) return null;

        if (in_array($value, $this->postTypes)) {
            return $value;
        }
        return null;
    }

    public function getDeliveoExported($source, $key, $default = null)
    {
        $value = $this->getValueFromGlobalArray($source, $key);
        if ($value === null) return null;

        if (in_array($value, array('not_exported', 'exported'))) {
            return $value;
        }
        return null;
    }

    public function getOrderArray($source, $arrayKey, $default = [])
    {
        $args = array(
            $arrayKey =>
            array(
                'flags' => FILTER_REQUIRE_ARRAY,
            )
        );
        $result = filter_input_array($source, $args);
        if ($result === false || $result === null) return $default;

        foreach ($result[$arrayKey] as $key => $value) {
            $result[$arrayKey][$key] = filter_var_array($value, array(
                'id' => FILTER_VALIDATE_INT,
                'option' => FILTER_VALIDATE_INT,
                'unit' => FILTER_VALIDATE_INT,
            ));
            if ($result[$arrayKey][$key] === null || $result[$arrayKey][$key] === false) {
                unset($result[$arrayKey][$key]);
            }
        }

        return ($result[$arrayKey] === null || $result[$arrayKey]) === false ? $default : $result[$arrayKey];
    }

    public function getIntegerArray($source, $key, $default = [])
    {
        $args = array(
            $key =>
            array(
                'filter' => FILTER_VALIDATE_INT,
                'flags' => FILTER_REQUIRE_ARRAY,
            )
        );
        $result = filter_input_array($source, $args);
        return ($result[$key] === null || $result[$key]) === false ? $default : $result[$key];
    }

    private function filterByRegex($value, $regexPattern)
    {
        return preg_replace($regexPattern, '', $value);
    }

    public function filterBasicCode($source, $key)
    {
        $value = filter_input($source, $key);
        if ($value === null || $value === false) return null;

        return $this->filterByRegex($value, "/[^0-9a-zA-Z-\/]+/");
    }

    public function getDeliveoSettings($source, $key)
    {
        $defaultText = array(
            "filter" => FILTER_VALIDATE_REGEXP,
            "options" => array("regexp" => "/[a-zA-Z0-9-\/ .]/")
        );

        $args = array(
            'api_key' => array(
                "filter" => FILTER_VALIDATE_REGEXP,
                "options" => array("regexp" => "/[a-zA-Z0-9]/")
            ),
            'licence_key' => array(
                "filter" => FILTER_VALIDATE_REGEXP,
                "options" => array("regexp" => "/[a-zA-Z0-9-\/]/")
            ),
            'sender' => $defaultText,
            'sender_country_code' => array(
                "filter" => FILTER_VALIDATE_REGEXP,
                "options" => array("regexp" => "/[a-zA-Z]{2}/")
            ),
            'sender_zip' => FILTER_SANITIZE_NUMBER_INT,
            'sender_city' => $defaultText,
            'sender_address' => $defaultText,
            'sender_apartment' => $defaultText,
            'sender_phone' => array(
                "filter" => FILTER_VALIDATE_REGEXP,
                "options" => array("regexp" => "/[+0-9]+/")
            ),
            'sender_email' => FILTER_VALIDATE_EMAIL,
            'x' => FILTER_DEFAULT,
            'y' => FILTER_DEFAULT,
            'z' => FILTER_DEFAULT,
            'priority' => FILTER_SANITIZE_NUMBER_INT,
            'saturday' => FILTER_SANITIZE_NUMBER_INT,
            'insurance' => FILTER_DEFAULT,
            'freight' => FILTER_DEFAULT,
            'delivery' => FILTER_SANITIZE_NUMBER_INT,
            'deliveo_settings' => FILTER_DEFAULT,
            'reference_id_is_order_id' => FILTER_SANITIZE_NUMBER_INT,
            'delivered_status' => FILTER_DEFAULT,
            'send_trigger' => FILTER_DEFAULT,
            'currency_multiplier' => FILTER_DEFAULT,
            'tracking_id_is_order_id' => FILTER_SANITIZE_NUMBER_INT,
            'shipping_options' => FILTER_DEFAULT,
            'packaging_unit' => FILTER_SANITIZE_NUMBER_INT,
            'dropoff_point_field' => FILTER_DEFAULT,
        );

        $result = filter_input_array($source, $args);
        return ($result === null || $result) === false ? null : $result;
    }
}
