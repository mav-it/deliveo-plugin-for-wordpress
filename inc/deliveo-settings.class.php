<?php

class MAV_IT_Deliveo_Settings
{
    public static $deliveoSettingsKeyList = array(
        'api_key',
        'licence_key',
        'sender',
        'sender_country_code',
        'sender_zip',
        'sender_city',
        'sender_address',
        'sender_apartment',
        'sender_phone',
        'sender_email',
        'x',
        'y',
        'z',
        'priority',
        'saturday',
        'insurance',
        'freight',
        'delivery',
        'deliveo_settings',
        'send_trigger',
        'reference_id_is_order_id',
        'delivered_status',
        'currency_multiplier',
        'tracking__id_is_order_id',
        'shipping_options',
        'packaging_unit',
        'dropoff_point_field',
    );
    public $deliveo_settings;
    public function __construct()
    {
        $this->deliveo_settings = $this->get_deliveo_settings();
        add_action('admin_menu', array($this, 'settings_page'), 99);

        add_action('admin_notices', array($this, 'missing_deliveo_settings_message'));
        add_action('wp_ajax_save_api_key_and_licence', array($this, 'save_api_key_and_licence'));
    }

    public static function initMenuItem()
    {
        $deliveo_settings_obj = new MAV_IT_Deliveo_Settings();
        $deliveo_settings_obj->settings_page_content();
    }

    private function init_deliveo_settings()
    {
        $init_settings = [];
        foreach (self::$deliveoSettingsKeyList as $key => $value) {
            $init_settings[$key] = '';
        }

        $init_settings = json_encode($init_settings);
        update_option('mav_it_deliveo_settings', $init_settings);
        $this->deliveo_settings = $init_settings;

        return $init_settings;
    }

    /* Add Deliveo settings page Woocommerce submenu */
    public function settings_page()
    {
        add_submenu_page('woocommerce', 'Deliveo', 'Deliveo', 'manage_options', 'deliveo-settings', array($this, 'settings_page_content'));
    }

    /* Settings page content (Save settings form) */
    public function settings_page_content()
    {
        $this->save_deliveo_settings();
        $settings = $this->get_deliveo_settings();
        $shipping_options_selector = $this->shipping_options_selector($settings);
        $order_status_selector = $this->order_status_selector($settings);
        $send_trigger_selector = $this->send_trigger_selector($settings);

        /* Declare variables because of undefined index errors, or create a parser function */
        if ((strlen($shipping_options_selector) > 200)) {
            $packagesettings = '<tr>
            <td colspan="2"><h2>' . __('Feladó beállítása | <small>Az itt megadott paraméterek kerülnek a csomag adataiba mint "Feladó".', 'mav-it') . '</small></h2></td>
        </tr>
        <tr>
            <td>' . __('Feladó neve', 'mav-it') . '</td>
            <td><input required type="text" name="sender" value="' . $settings['sender'] . '" class="required" data-message="' . __('Feladó név kötelező', 'mav-it') . '" /></td>
        </tr>
        <tr>
            <td>' . __('Feladó országának kétjegyű kódja. Pl: "HU", "DE"', 'mav-it') . '</td>
            <td><input required type="text" maxlength="2" name="sender_country_code" value="' . $settings['sender_country_code'] . '" class="required" data-message="' . __('Ország kód kötelező', 'mav-it') . '" /></td>
        </tr>
        <tr>
            <td>' . __('Feladó településének az irányítószáma', 'mav-it') . '</td>
            <td><input required type="text" name="sender_zip" value="' . $settings['sender_zip'] . '" class="required" data-message="' . __('Feladó irányítószám kötelező', 'mav-it') . '" /></td>
        </tr>
        <tr>
            <td>' . __('Feladó település neve', 'mav-it') . '</td>
            <td><input required type="text" name="sender_city" value="' . $settings['sender_city'] . '" class="required" data-message="' . __('Település neve kötelező', 'mav-it') . '" /></td>
        </tr>
        <tr>
            <td>' . __('Feladó közterület neve, házszám', 'mav-it') . '</td>
            <td><input required type="text" name="sender_address" value="' . $settings['sender_address'] . '" class="required" data-message="' . __('Feladó közterület név kötelező', 'mav-it') . '" /></td>
        </tr>
        <tr>
            <td>' . __('Feladó épület, lépcsőház, emelet, ajtó', 'mav-it') . '</td>
            <td><input type="text" name="sender_apartment" value="' . $settings['sender_apartment'] . '" /></td>
        </tr>
        <tr>
            <td>' . __('Feladó telefonszám', 'mav-it') . '</td>
            <td><input required type="text" name="sender_phone" value="' . $settings['sender_phone'] . '" class="required" data-message="' . __('Feladó telefonszám kötelező', 'mav-it') . '" /></td>
        </tr>
        <tr>
            <td>' . __('Feladó email', 'mav-it') . '</td>
            <td><input required type="text" name="sender_email" value="' . $settings['sender_email'] . '" class="required" data-message="' . __('Feladó email kötelező', 'mav-it') . '" /></td>
        </tr>
        <tr>
            <td colspan="2"><h2>' . __('Alapértelmezett csomag méretek | <small>Ha hiányoznak a termék méretei, akkor ezeket használjuk feladáskor.', 'mav-it') . '</small></h2></td>
        </tr>
        <tr>
            <td>' . __('X = szélesség, Y = magasság, Z = mélység (cm)', 'mav-it') . '</td>
            <td>X: <input required type="number" name="x" value="' . $settings['x'] . '" class="required" data-message="' . __('Alapértelmezett csomag magasság kötelező', 'mav-it') . '" /> Y: <input type="number" name="y" value="' . $settings['y'] . '" class="required" data-message="' . __('Alapértelmezett csomag szélesség kötelező', 'mav-it') . '" /> Z: <input type="number" name="z" value="' . $settings['z'] . '" class="required" data-message="' . __('Alapértelmezett csomag mélység kötelező', 'mav-it') . '" /></td>
        </tr>
                <tr>
            <td colspan="2"><h2>' . __('Utánvét korrekció', 'mav-it') . '</h2></td>
        </tr>
        <td>' . __('Korrekciós szorzó', 'mav-it') . '</td>
            <td><input required type="number" min="1" step="0.0001" name="currency_multiplier" value="' . ($settings['currency_multiplier'] ?? 1) . '" style="width:500px;" class="required" data-message="' . __('Korrekciós mező kötelező', 'mav-it') . '" />
            <br><small>' . __('Amennyiben webáruháza rendeléseit nem forintban kezeli, kérjük használja ez a mezőt. (pl.: EUR esetében ~0,003 értéket adjon meg.)', 'mav-it') . '</small>
            </td>
        <tr>
            <td colspan="2"><h2>' . __('Szállítási paraméterek | <small>Ezekkel a paraméterekkel lesz feladva minden csomag.</small>', 'mav-it') . '</h2></td>
        </tr>
        <tr>
            <td>' . __('Opcionális paraméter #1', 'mav-it') . '</td>
            <td>
                <input type="hidden" name="priority" value="0" />
                <input type="checkbox" name="priority" value="1" ' . mitd_is_option_checked($settings['priority']) . ' />
            </td>
        </tr>
        <tr>
            <td>' . __('Opcionális paraméter #2', 'mav-it') . '</td>
            <td>
                <input type="hidden" name="insurance" value="0" />
                <input type="text" name="insurance" value="' . $settings['insurance'] . '"  />
            </td>
        </tr>
        <tr>
            <td>' . __('Opcionális paraméter #3', 'mav-it') . '</td>
            <td>
                <input type="hidden" name="saturday" value="0" />
                <input type="checkbox" name="saturday" value="1" ' . mitd_is_option_checked($settings['saturday']) . ' />
            </td>
        </tr>
        <tr>
            <td>' . __('Rendelészám kerüljön a Hivatkozási szám mezőbe', 'mav-it') . '</td>
            <td>
                <input type="hidden" name="reference_id_is_order_id" value="0" />
                <input type="checkbox" name="reference_id_is_order_id" value="1" ' . mitd_is_option_checked($settings['reference_id_is_order_id']) . ' /> ' . __('Használat előtt egyeztessen a szerződött futárszolgálattal!', 'mav-it') . '
            </td>
        </tr>
        <tr>
            <td>' . __('Rendelésszám kerüljön a Követőkód mezőbe', 'mav-it') . '</td>
            <td>
                <input type="hidden" name="tracking_id_is_order_id" value="0" />
                <input type="checkbox" name="tracking_id_is_order_id" value="1" ' . mitd_is_option_checked($settings['tracking_id_is_order_id']) . ' />
            </td>
        </tr>
        <tr>
            <td>' . __('Ki fizeti a szállítási költséget?', 'mav-it') . '</td>
            <td>
                <label for="freight-felado">' . __('Feladó', 'mav-it') . '</label>
                <input type="radio" name="freight" value="felado" id="freight-felado" ' . mitd_is_radio_checked($settings['freight'], 'felado') . ' />

                <label for="freight-cimzett">' . __('Címzett', 'mav-it') . '</label>
                <input type="radio" name="freight" value="cimzett" id="freight-cimzett" ' . mitd_is_radio_checked($settings['freight'], 'cimzett') . ' />
            </td>
        </tr>
        <tr>
            <td>' . __('Adattovábbítás utáni állapot:', 'mav-it') . '</td>
            <td>' . $order_status_selector . ' 
                        <br><small>' . __('Adattovábbítás után a rendelések állapotát erre az állapotra módosítja.', 'mav-it') . '</small>

            </td>
        </tr>
        <tr>
            <td>' . __('Automatikus továbbítás állapota:', 'mav-it') . '</td>
            <td>' . $send_trigger_selector . '
            <br><small>' . __('Adattovábbítás a Deliveo rendszerébe, ha a küldemény a kiválasztott állapotba kerül', 'mav-it') . '</small>
            
            </td>
        </tr>
         <tr>
            <td>' . __('Átvételi pont', 'mav-it') . '</td>
            <td>
            <input type="text" name="dropoff_point_field" value="' . $settings['dropoff_point_field'] . '" />
            <br><small>' . __('Egyedi szállítási modul esetén adjuk meg, hogy a rendelés melyik paramétere tárolja az átvételi pontot', 'mav-it') . '</small>

            </td>
        </tr>
        <tr>
            <td colspan="2"><h2>Colli</small></h2></td>
        </tr>
        <tr>
            <td>' . __('Csomagok száma', 'mav-it') . '</td>
            <td><select name="packaging_unit">
                <option ' . ($settings['packaging_unit'] == 0 ? 'selected' : '') . ' value="0">' . __('Mindig egy', 'mav-it') . '</option>
                <option ' . ($settings['packaging_unit'] == 1 ? 'selected' : '') . ' value="1">' . __('Tételenként egy', 'mav-it') . '</option>
                <option ' . ($settings['packaging_unit'] == 2 ? 'selected' : '') . ' value="2">' . __('Mindig manuálisan állítom be', 'mav-it') . '</option>
            </select>
            </td>
        </tr>
        <tr>
            <td colspan="2"><h2>' . __('Szállítási opció', 'mav-it') . '</small></h2></td>
        </tr>
        <tr>
            <td>' . __('Alapértelmezett szállítási opció:', 'mav-it') . '</td>
            <td>' . $shipping_options_selector . '</td>
        </tr>
        <tr>
            <td></td>
            <td><button name="deliveo_settings" type="submit" class="button button-primary deliveo_settings_save">' . __('Mentés', 'mav-it') . '</button></td>
        </tr>';
        } else {
            $this->admin_notice_api_error();
            $packagesettings = '';
        }
        $content = '
        <h1>' . __('Beállítások', 'mav-it') . '</h1>

        <form action="" method="post" id="deliveo-settings-form" class="deliveo-settings-form">
            <div class="validation-messages hidden"></div>
            <input type="hidden" name="_nonce" value="' . wp_create_nonce('connection-return') . '" />
            <input type="hidden" name="_url" value="' . get_site_url() . '" />
            <table>
                <tr>
                    <td>' . __('API kulcs (a futárszolgálattól kérd el)', 'mav-it') . '</td>
                    <td>
                    <input type="text" id="deliveo-api-key" name="api_key" value="' . $settings['api_key'] . '" class="required" data-message="' . __('API kulcs kötelező', 'mav-it') . '" />
                    </td>
                </tr>
                <tr>
                    <td>' . __('Licensz (a szerződött futárszolgálat adja meg)', 'mav-it') . '</td>
                    <td>
                        <input type="text" id="deliveo-licence-key" name="licence_key" value="' . $settings['licence_key'] . '" class="required" data-message="' . __('Licence kötelező', 'mav-it') . '" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button name="deliveo_settings" type="submit" class="button button-primary deliveo_settings_save">' . __('Mentés', 'mav-it') . '</button>
                    </td>
                </tr>
                '
            . $packagesettings .
            '</table>
        </form>';

        echo $content;
    }

    /* Get Deliveo settings from DB and parse to array */
    public function get_deliveo_settings()
    {

        $settings = get_option('mav_it_deliveo_settings', '');
        if (empty($settings)) {
            $settings = $this->init_deliveo_settings();
        }

        return json_decode($settings, true);
    }

    /* Parse Deliveo options array to json strings and save to DB */
    public function save_deliveo_settings()
    {
        $deliveoSettings = Deliveo_Request_Filter::getInstance()
            ->getDeliveoSettings(INPUT_POST, 'deliveo_settings');
        $settings = $this->get_deliveo_settings();
        if ($deliveoSettings !== null) {
            foreach ($deliveoSettings as $key => $value) {
                // if ($value !== null && $value !== '') {
                $settings[$key] = $value;
                // }
            }

            $deliveoSettings = $settings;
            $deliveo_api = new Deliveo_API(new MAV_IT_Deliveo_Settings());
            $shipping_options = $deliveo_api->get_shipping_options();
            $deliveoSettings['shipping_options'] = $shipping_options;

            $settings = json_encode($deliveoSettings);
            update_option('mav_it_deliveo_settings', $settings);
        }

        $action = Deliveo_Request_Filter::getInstance()
            ->filterBasicCode(INPUT_POST, 'action');

        if ($action !== null && $action == 'save_api_key_and_licence') {
            $settings = $this->get_deliveo_settings();
            $settings['api_key'] = Deliveo_Request_Filter::getInstance()
                ->filterBasicCode(INPUT_POST, 'api_key');
            $settings['licence_key'] = Deliveo_Request_Filter::getInstance()
                ->filterBasicCode(INPUT_POST, 'licence_key');

            $this->admin_notice__success();

            $settings = json_encode($settings);
            update_option('mav_it_deliveo_settings', $settings);
        }
    }

    /** Build a selector by Shipping options details from DELIVEO API */
    public function shipping_options_selector($deliveo_settings)
    {
        $deliveo_api = new Deliveo_API(new MAV_IT_Deliveo_Settings());
        $shipping_options = $deliveo_api->get_shipping_options();

        $api_key = mitd_get_value($deliveo_settings['api_key']);
        $saved_shipping = mitd_get_value($deliveo_settings['delivery']);
        $selector = '';

        if (empty($api_key)) {
            $selector = '<input type="hidden" name="delivery" value="" />';
            $selector .= __('A szállítási opció beállításához kérjük előbb adja meg az API kulcsot és mentse el a beállításokat', 'mav-it');
        } else {
            $selector = '
             <select name="delivery" class="required" data-message="' . __('Szállítási opció kötelező', 'mav-it') . '">
                 <option value="">' . __('Válasszon szállítási opciót', 'mav-it') . '</option>';

            foreach ($shipping_options as $shipping_option) {
                $selected = mitd_is_selector_selected($shipping_option->value, $saved_shipping);

                $selector .= '
                <option value="' . $shipping_option->value . '" ' . $selected . '>' . $shipping_option->description . '</option>';
            }

            $selector .= '
            </select>';
        }

        return $selector;
    }
    public function shipping_options_menu()
    {
        $settings = $this->init_deliveo_settings();
        return ((array) $settings['shipping_options']);
    }

    public function order_status_selector($deliveo_settings)
    {
        $status_types = wc_get_order_statuses();
        $delivered_status = $deliveo_settings['delivered_status'];

        $selector = '
        <select name="delivered_status">
            <option value="">' . __('Nem változtat', 'mav-it') . '</option>';

        foreach ($status_types as $status_key => $status_value) {
            $selected = '';

            if ($delivered_status == $status_key) {
                $selected = 'selected="selected"';
            }

            $selector .= '
            <option value="' . $status_key . '" ' . $selected . '>' . $status_value . '</option>';
        }

        $selector .= '
        </select>';

        return $selector;
    }

    public function send_trigger_selector($deliveo_settings)
    {
        $status_types = wc_get_order_statuses();
        $send_trigger_selector = $deliveo_settings['send_trigger'];

        $selector = '
        <select name="send_trigger">
            <option value="">' . __('Egyik sem', 'mav-it') . '</option>';

        foreach ($status_types as $status_key => $status_value) {
            $selected = '';

            if ($send_trigger_selector == $status_key) {
                $selected = 'selected="selected"';
            }

            $selector .= '
            <option value="' . $status_key . '" ' . $selected . '>' . $status_value . '</option>';
        }

        $selector .= '
        </select>';

        return $selector;
    }

    /* Check if some important details are missing from deliveo settings: API key, Licence key,  */
    public function deliveo_setting_missing()
    {
        $settings = $this->deliveo_settings;
        $setting_missing = false;

        foreach ($settings as $setting_key => $setting_value) {
            if (
                // kizárunk az ellenőrzésből pár paramétert
                $setting_key != 'deliveo_settings' &&
                $setting_key != 'sender_apartment' &&
                $setting_key != 'delivered_status' &&
                $setting_key != 'send_trigger' &&
                $setting_key != 'shipping_options' &&

                //a többi értéke nem lehet 0
                strlen((string)$setting_value) < 0
            ) {
                $setting_missing = true;
            }
            // teszteléshez a mezők értékeinek kiíratása
            //echo $setting_key." - ".$setting_value."<br>";
        }

        return $setting_missing;
    }

    public function missing_deliveo_settings_message()
    {
        $setting_missing = $this->deliveo_setting_missing();

        if ($setting_missing) {
            $class = 'notice notice-warning';
            $message = __('Hiányzó Deliveo beállítás! Az export használatához kérjük menjen a Woocommerce -> Deliveo oldalra a hiányzó adatok megadásához', 'mav-it');

            printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), esc_html($message));
        }
    }

    public function set_delivered_order_status($order_id)
    {
        global $wpdb;

        $settings = $this->deliveo_settings;
        $delivered_status = $settings['delivered_status'];
        if (!empty($delivered_status)) {
            //update order status with delivered status
            $order = wc_get_order($order_id);
            $order->update_status($delivered_status);
        }
    }

    public function admin_notice__success()
    {

        $class = 'notice notice-success is-dismissible';
        $message = __('Beállítások mentve, authentikációs adatok módosításakor ellenőrizzük a szállítási opciókat!', 'mav-it');

        printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), esc_html($message));
    }

    public function admin_notice_api_error()
    {

        $class = 'notice notice-warning is-dismissible';
        $message = __('Sikertelen csatlakozás, kérjük ellenőrizze a licenc adatokat!', 'mav-it');

        printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), esc_html($message));
    }

    public function save_api_key_and_licence()
    {
        $this->save_deliveo_settings();
        wp_die();
    }
}
