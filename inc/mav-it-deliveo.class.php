<?php

class MAV_IT_Deliveo
{
    public $deliveo_settings_obj;
    public $deliveo_settings;
    public $export_details;
    public $export_details_for_api;
    public $export_allowed;
    public $admin_orders_url;


    public function __construct($deliveo_settings_obj = null)
    {
        $this->deliveo_settings_obj   = $deliveo_settings_obj;
        $this->deliveo_settings       = $this->deliveo_settings_obj->get_deliveo_settings();
        $this->export_details         = array();
        $this->export_details_for_api = array();
        $this->export_allowed         = false;
        $this->admin_orders_url       = get_bloginfo('url') . '/wp-admin/edit.php?post_type=shop_order';

        add_action('init', array($this, 'export_when_logged_in'));
        add_action('admin_enqueue_scripts', array($this, 'footer_scripts'));
        add_action('admin_enqueue_scripts', array($this, 'header_scripts'));
        add_action('wp_ajax_label_download', array($this, 'label_download'));
        add_action('wp_ajax_view_signature', array($this, 'view_signature'));
        add_action('wp_ajax_view_package_log', array($this, 'view_package_log'));
        add_action('wp_ajax_package_details', array($this, 'package_details'));
        add_action('wp_ajax_api_custom_send', array($this, 'api_custom_send'));

        //add wp nonce
        add_action('admin_init', array($this, 'add_nonce'));

        add_action('admin_notices', array($this, 'deliveo_success_msg'));
    }

    public function add_nonce()
    {
        wp_nonce_field('deliveo_nonce_action', 'deliveo_nonce');
    }

    public function footer_scripts($hook)
    {
        $post_type = get_query_var('post_type', '');
        wp_register_script('deliveo-admin-js', MAV_IT_DELIVEO_DIR_URL . 'js/admin.js', array(), false, true);
        wp_enqueue_script('deliveo-admin-js');

        if ($hook == 'woocommerce_page_deliveo-settings') {
            wp_register_script('deliveo-validation-js', MAV_IT_DELIVEO_DIR_URL . 'js/validation.js', array(), false, true);
            wp_enqueue_script('deliveo-validation-js');

            wp_register_script('deliveo-admin-js', MAV_IT_DELIVEO_DIR_URL . 'js/admin.js', array(), false, true);
            wp_enqueue_script('deliveo-admin-js');
        }

        if ($hook == 'edit.php' && $post_type == 'shop_order') {
            wp_register_script('deliveo-export-js', MAV_IT_DELIVEO_DIR_URL . 'js/export.js', array(), false, true);
            wp_enqueue_script('deliveo-export-js');
        }
    }

    public function header_scripts()
    {
        wp_register_style('deliveo-admin-css', MAV_IT_DELIVEO_DIR_URL . 'css/mav-it-deliveo.css', false, '1.0.0');
        wp_enqueue_style('deliveo-admin-css');
    }

    public function generate_csv($orders)
    {

        $order_items = $this->get_export_details($orders);

        $csv_builder = new MAV_IT_CSV_Builder($order_items);
        $csv_content = $csv_builder->build_csv();

        $csv_export = new MAV_IT_CSV_Export();
        $csv_export->export($csv_content);
    }

    public function send_by_api($orderID, $shipping = false, $unit = false)
    {
        $settings       = $this->deliveo_settings;
        $export_allowed = $this->export_allowed;

        if (($unit)) {
            $packaging_unit = $unit;
        } else {
            $packaging_unit = $this->getPackagingUnit($orderID);
        }

        $group_code = get_metadata('post', $orderID, '_group_code', true);
        if (strlen($group_code) == 0) {
            $order_id = $orderID;

            //get order details
            $order_details = new WC_Order($order_id);
            $order_items   = $order_details->get_items();



            if (get_metadata('post', $order_id, '_deliveo_exported', true) != "true") {
                $shipping     = $shipping ? $shipping : $settings['delivery'];
                $cod          = $this->get_cod($order_id);
                $insurance    = (int) $settings['insurance'];
                $currentOrder = new WC_Order($order_id);
                $cartProducts = array();
                $comment      = $currentOrder->get_customer_note() . ' ';

                foreach ($currentOrder->get_items() as $item_id => $item_data) {
                    $product = $item_data->get_product();
                    $sku     = $product->get_sku();
                    for ($i = 0; $i < $item_data->get_quantity(); $i++) {
                        $weight = $product->get_weight();
                        if ($weight == '') {
                            $weight = 1;
                        }
                        $x = $product->get_width();
                        if ($x == '') {
                            $x = $settings['x'];
                        }
                        $y = $product->get_height();
                        if ($y == '') {
                            $y = $settings['y'];
                        }
                        $z = $product->get_length();
                        if ($z == '') {
                            $z = $settings['z'];
                        }

                        $cartProducts[] = array(
                            "x"       => $x ?: 1,
                            "y"       => $y ?: 1,
                            "z"       => $z ?: 1,
                            "weight"  => $weight ?: 1,
                            "item_no" => $sku ?: '',
                        );
                    }
                }

                $consignee = $order_details->get_shipping_last_name() . " " . $order_details->get_shipping_first_name();
                if ($order_details->get_shipping_company()) {
                    $consignee = $consignee . " - " . $order_details->get_shipping_company();
                }
                $package = array(
                    'sender'              => $settings['sender'],
                    'sender_country'      => $settings['sender_country_code'],
                    'sender_zip'          => $settings['sender_zip'],
                    'sender_city'         => $settings['sender_city'],
                    'sender_address'      => $settings['sender_address'],
                    'sender_apartment'    => $settings['sender_apartment'],
                    'sender_phone'        => $settings['sender_phone'],
                    'sender_email'        => $settings['sender_email'],
                    'consignee' => $consignee,
                    'consignee_country'   => $order_details->get_shipping_country(),
                    'consignee_zip'       => $order_details->get_shipping_postcode(),
                    'consignee_city'      => $order_details->get_shipping_city(),
                    'consignee_address'   => $order_details->get_shipping_address_1(),
                    'consignee_apartment' => $order_details->get_shipping_address_2(),
                    'consignee_phone'     => $order_details->get_billing_phone() ?? $order_details->get_shipping_phone(),
                    'consignee_email'     => $order_details->get_billing_email(),
                    'delivery'            => $shipping,
                    'priority'            => $settings['priority'],
                    'saturday'            => $settings['saturday'],
                    'insurance'           => $insurance,
                    'referenceid'         => $this->get_tracking_id($order_id),
                    'cod'                 => $cod * ($settings['currency_multiplier'] ?? 1),
                    'currency'            => get_option('woocommerce_currency'),
                    'freight'             => $settings['freight'],
                    'comment'             => $comment,
                    'tracking'            => $this->get_tracking_id($order_id),
                    'colli'      => $packaging_unit,
                    'packages'            => $cartProducts,
                );

                if (isset($settings['dropoff_point_field']) && strlen($settings['dropoff_point_field']) > 0) {
                    $shop_id = explode('|', $order_details->get_meta($settings['dropoff_point_field']));
                    if (sizeof($shop_id) == 3) {
                        $package['shop_id'] = end($shop_id);
                    } else {
                        $package['shop_id'] = $order_details->get_meta($settings['dropoff_point_field']);
                    }
                }

                $deliveo_api      = new Deliveo_API($this->deliveo_settings_obj);
                $deliveo_progress = $deliveo_api->send_order_items($order_id, $package, $export_allowed);
                return $deliveo_progress;
            }
        }
    }

    public function getPackagingUnit($order_id)
    {
        $packaging_unit = json_decode(get_option('mav_it_deliveo_settings'))->packaging_unit;
        $order          = new WC_Order($order_id);

        switch ((int) $packaging_unit) {
            case 0:
                return 1;
                break;
            case 1:
                return $order->get_item_count();
                break;
            case 2:
                return false;
                break;

            default:
                return false;
                break;
        }
    }

    public function deliveo_success_msg()
    {
        $deliveoOk = Deliveo_Request_Filter::getInstance()
            ->getFilteredVar(INPUT_POST, 'deliveo_ok');
        $succeded = Deliveo_Request_Filter::getInstance()
            ->getFilteredVar(INPUT_GET, 'succeded');
        $failed = Deliveo_Request_Filter::getInstance()
            ->getFilteredVar(INPUT_GET, 'failed');
        $failedMessages = Deliveo_Request_Filter::getInstance()
            ->getFilteredVar(INPUT_GET, 'failed_messages');
        $deliveoExportFailed = Deliveo_Request_Filter::getInstance()
            ->getFilteredVar(INPUT_GET, 'deliveo_export_failed');

        if ($deliveoOk) {
            echo '<div class="notice notice-success is-dismissible">
            <p><strong><h2>' . __('Deliveo csomagfeladás befejezve', 'mav-it') . '</h2></strong> ' . __('Sikeres', 'mav-it') . ': ' . $succeded . ', ' . __('sikertelen') . ': ' . $failed . '.</p>
            </div>';
        }
        if ($failedMessages) {
            foreach ($failedMessages as $msg) {
                echo '<div class="notice notice-error is-dismissible">
                <p>' . $msg . '</p>
                </div>';
            }
        }

        if ($deliveoExportFailed) {
            echo '<div class="notice notice-error is-dismissible">
                 <p><strong><h2>' . __('Deliveo Export hiba', 'mav-it') . '</h2></strong>' . __('Nem sikerült exportálni a rendeléseket. A korábban feladott rendeléseket már nem tölthetjük le!', 'mav-it') . '</p>
             </div>';
        }
    }

    public function export_when_logged_in()
    {
        if (is_user_logged_in()) {
            // $this->generate_csv();
            // $this->send_by_api(null, null, null);
        }
    }

    /* Details for exports by order ids. If the second param is true, the data structure is for API call */
    public function get_export_details($order_ids, $for_api = false)
    {
        global $wpdb;

        foreach ($order_ids as $order_id) {

            $this->set_order_item_details($order_id);
        }
        if ($for_api) {
            return $this->export_details_for_api;
        } else {
            return $this->export_details;
        }
    }

    public function init_export_details_for_api($order_id)
    {
        $generateDeliveoCsv = Deliveo_Request_Filter::getInstance()
            ->getFilteredVar(INPUT_GET, 'generate_deliveo_csv');

        $allSelected = explode(',', $generateDeliveoCsv);
        $shipping    = '';

        foreach ($allSelected as $selected) {
            $details = explode('-', $selected);
            if ($details[0] == $order_id) {
                $shipping = $details[1];
            }
        }

        $settings  = $this->deliveo_settings;
        $cod       = $this->get_cod($order_id);
        $insurance = (int) $settings['insurance'];

        if ($insurance == 1) {
            $insurance = get_metadata('post', $order_id, '_order_total', true);
        }

        $this->export_details_for_api['order_id_' . $order_id] = array(
            'sender'              => $settings['sender'],
            'sender_country'      => $settings['sender_country_code'],
            'sender_zip'          => $settings['sender_zip'],
            'sender_city'         => $settings['sender_city'],
            'sender_address'      => $settings['sender_address'],
            'sender_apartment'    => $settings['sender_apartment'],
            'sender_phone'        => $settings['sender_phone'],
            'sender_email'        => $settings['sender_email'],
            'consignee'           => get_metadata('post', $order_id, '_shipping_first_name', true) . ' ' . get_metadata('post', $order_id, '_shipping_last_name', true),
            'consignee_country'   => 'HU',
            'consignee_zip'       => get_metadata('post', $order_id, '_shipping_postcode', true),
            'consignee_city'      => get_metadata('post', $order_id, '_shipping_city', true),
            'consignee_address'   => get_metadata('post', $order_id, '_shipping_address_1', true),
            'consignee_apartment' => get_metadata('post', $order_id, '_shipping_address_2', true),
            'consignee_phone'     => get_metadata('post', $order_id, '_billing_phone', true),
            'consignee_email'     => get_metadata('post', $order_id, '_billing_email', true),
            'delivery'            => $shipping, //int(10) 'Szállítási opció',
            'optional_parameter_1'            => $settings['priority'], //int(1) 'Elsőbbségi kézbesítés',
            'optional_parameter_3'            => $settings['saturday'], //int(1) 'Szombati késbesítés',
            'optional_parameter_2'           => $insurance,
            'referenceid'         => $this->get_reference_id($order_id),
            'cod'                 => $cod, //decimal(10,2) 'Utánvét összege',
            'freight'             => $settings['freight'],
            'comment'             => ' ',
            'packages'            => array(),
        );
    }

    /* Set the details for CSV and API */
    public function set_order_item_details($order_id)
    {
        global $wpdb;

        $order_lines      = array();
        $deliveo_settings = $this->deliveo_settings;

        $query = 'SELECT * FROM ' . $wpdb->prefix . 'woocommerce_order_items woi
            JOIN ' . $wpdb->prefix . 'woocommerce_order_itemmeta woim
            ON (woi.order_item_id = woim.order_item_id)
            JOIN ' . $wpdb->prefix . 'posts p
            ON (woim.meta_value = p.ID)
            WHERE woi.order_id = "' . $order_id . '" AND woim.meta_key = "_product_id"';

        $results  = $wpdb->get_results($query);
        $exported = get_metadata('post', $order_id, '_deliveo_exported', true);
        $comment  = $this->get_order_comment($order_id) . ' ';

        if ($exported != 'true') {
            $this->init_export_details_for_api($order_id);

            foreach ($results as $result) {
                $weight  = mitd_post_meta($result->ID, '_weight', 1);
                $x       = mitd_post_meta($result->ID, '_length', $deliveo_settings['x']);
                $y       = mitd_post_meta($result->ID, '_width', $deliveo_settings['y']);
                $z       = mitd_post_meta($result->ID, '_height', $deliveo_settings['z']);
                $item_no = get_metadata('post', $result->ID, '_sku', true);
                $cod     = $this->get_cod($order_id[0]);
                $qty     = (int) wc_get_order_item_meta($result->order_item_id, '_qty', true);

                $this->export_allowed = true;

                for ($i = 0; $i < $qty; $i++) {
                    //Do not modify the order of this array item list. This array match for CSV header
                    $this->export_details[] = array(
                        'saturday'            => $deliveo_settings['saturday'],
                        'referenceid'         => $this->get_reference_id($order_id[0]),
                        'cod'                 => $cod,
                        'sender_id'           => '',
                        'sender'              => $deliveo_settings['sender'],
                        'sender_country_code' => $deliveo_settings['sender_country_code'],
                        'sender_zip'          => $deliveo_settings['sender_zip'],
                        'sender_city'         => $deliveo_settings['sender_city'],
                        'sender_address'      => $deliveo_settings['sender_address'],
                        'sender_apartment'    => $deliveo_settings['sender_apartment'],
                        'sender_phone'        => $deliveo_settings['sender_phone'],
                        'sender_email'        => $deliveo_settings['sender_email'],
                        'consignee_id'        => '',
                        'consignee'           => get_metadata('post', $order_id[0], '_shipping_first_name', true) . ' ' . get_metadata('post', $order_id[0], '_shipping_last_name', true),
                        'consignee_zip'       => get_metadata('post', $order_id[0], '_shipping_postcode', true),
                        'consignee_city'      => get_metadata('post', $order_id[0], '_shipping_city', true),
                        'consignee_address'   => get_metadata('post', $order_id[0], '_shipping_address_1', true),
                        'consignee_apartment' => get_metadata('post', $order_id[0], '_shipping_address_2', true),
                        'consignee_phone'     => get_metadata('post', $order_id[0], '_billing_phone', true),
                        'consignee_email'     => get_metadata('post', $order_id[0], '_billing_email', true),
                        'weight'              => $weight,
                        'comment'             => $comment,
                        'deliveo_id'            => '',
                        'pick_up_point'       => '',
                        'x'                   => $x,
                        'y'                   => $y,
                        'z'                   => $z,
                        'customcode'          => '',
                        'item_no'             => $item_no,
                        'tracking'            => $this->get_tracking_id($order_id[0]),
                    );

                    $this->export_details_for_api['order_id_' . $order_id[0]]['comment']    = $comment;
                    $this->export_details_for_api['order_id_' . $order_id[0]]['packages'][] = array(
                        'weight'  => $weight,
                        'x'       => $x,
                        'y'       => $y,
                        'z'       => $z,
                        'item_no' => $item_no,
                    );
                }
            }
        }
    }

    /* if payment type is COD the cost will be the payment total */
    private function get_cod($order_id)
    {
        $payment = new WC_Order($order_id);
        $payment_type = $payment->get_payment_method();
        $cod = 0;

        if ($payment_type == 'cod') {
            $cod = $payment->get_total();
        }
        return $cod;
    }

    /** If user check the reference id equal to order id on Deliveo settings, the function return with order id as reference id */
    private function get_reference_id($order_id)
    {
        $settings                 = $this->deliveo_settings;
        $reference_id_is_order_id = (int) $settings['reference_id_is_order_id'];
        $reference_id             = '';

        if ($reference_id_is_order_id) {
            $reference_id = '#' . $order_id;
        }

        return $reference_id;
    }

    /** If user check the reference id equal to order id on Deliveo settings, the function return with order id as reference id */
    private function get_tracking_id($order_id)
    {
        $settings                = $this->deliveo_settings;
        $tracking_id_is_order_id = (int) $settings['tracking_id_is_order_id'];
        $tracking_id             = '';

        if ($tracking_id_is_order_id == 1) {
            $tracking_id = '#' . $order_id;
        }

        return $tracking_id;
    }

    /** Get order comment */
    private function get_order_comment($order_id)
    {
        global $wpdb;

        $query  = 'SELECT post_excerpt FROM ' . $wpdb->prefix . 'posts WHERE post_type = "shop_order" AND ID = "' . $order_id . '"';
        $result = $wpdb->get_row($query);

        return '';
        // return $result->post_excerpt;
    }

    public function label_download()
    {
        $groupId = Deliveo_Request_Filter::getInstance()
            ->filterBasicCode(INPUT_POST, 'deliveo_id');

        $settings    = $this->deliveo_settings;
        $label_url   = "https://api.deliveo.eu/label/" . $groupId . "?licence=" . $settings["licence_key"] . "&api_key=" . $settings["api_key"];
        $package_url = "https://api.deliveo.eu/package/" . $groupId . "?licence=" . $settings["licence_key"] . "&api_key=" . $settings["api_key"];

        $tmpfile = download_url($label_url, $timeout = 300);

        $check_signature = json_decode(wp_remote_fopen($package_url), true);
        if ($check_signature['data'][0]['deliveo_id'] == $groupId) {
            $permfile = $groupId . '.pdf';
            $destfile = wp_get_upload_dir()['path'] . "/" . $groupId . '.pdf';
            $dest_url = wp_get_upload_dir()['url'] . "/" . $groupId . '.pdf';
            copy($tmpfile, $destfile);
            unlink($tmpfile);
            $package =
                '<a target="blank" href="' . $dest_url . '"><img title="' . __('Csomagcímke letöltése', 'mav-it') . '"  style="vertical-align:middle;height:36px;" src="' . MAV_IT_DELIVEO_DIR_URL . 'images/deliveo-package-barcode.png" ></a>';
            echo $package;
        } else {
            $package =
                '<img title="' . __('Nincs csomagcímke', 'mav-it') . '"  style="vertical-align:middle;height:36px;filter:grayscale(100%);" src="' . MAV_IT_DELIVEO_DIR_URL . 'images/deliveo-package-barcode.png" >';
            echo $package;
        }
        wp_die();
    }

    public function view_signature()
    {
        $groupId = Deliveo_Request_Filter::getInstance()
            ->filterBasicCode(INPUT_POST, 'deliveo_id');

        $settings      = $this->deliveo_settings;
        $signature_url = "https://api.deliveo.eu/signature/" . $groupId . "?licence=" . $settings["licence_key"] . "&api_key=" . $settings["api_key"];

        $tmpfile = download_url($signature_url, $timeout = 300);

        $check_signature = json_decode(wp_remote_fopen($signature_url), true);
        if ($check_signature['type'] == 'error') {
            $response = array(
                "error" => "no_signature",
                "img"   => '<img title="' . __('Nincs aláíráslap', 'mav-it') . '"  style="vertical-align:middle;height:24px; filter: grayscale(100%);" src="' . MAV_IT_DELIVEO_DIR_URL . 'images/deliveo-signature.png" ></a>',
            );
            echo json_encode($response);
        } else {
            $permfile = $groupId . '_sign.pdf';
            $destfile = wp_get_upload_dir()['path'] . "/" . $groupId . '_sign.pdf';
            $dest_url = wp_get_upload_dir()['url'] . "/" . $groupId . '_sign.pdf';
            copy($tmpfile, $destfile);
            unlink($tmpfile);
            $package = array(
                'url' => $dest_url,
                'img' => '<a href="' . $dest_url . '" target="blank"><img title="' . __('Aláíráslap letöltése', 'mav-it') . ': ' . $groupId . '"  style="vertical-align:middle;height:24px;" src="' . MAV_IT_DELIVEO_DIR_URL . 'images/deliveo-signature.png" ></a>',
            );
            echo json_encode($package);
        }
        wp_die();
    }

    public function view_package_log()
    {
        $groupId = Deliveo_Request_Filter::getInstance()
            ->filterBasicCode(INPUT_POST, 'deliveo_id');
        $settings        = $this->deliveo_settings;
        $package_log_url = "https://api.deliveo.eu/package_log/" . $groupId . "?licence=" . $settings["licence_key"] . "&api_key=" . $settings["api_key"];

        $package_log = json_decode(wp_remote_fopen($package_log_url), true)['data'];

        if (isset($package_log[0])) {
            $row = '<h4>' . __('Napló', 'mav-it') . '</h4>';
        } else {
            $row = '<h4 style="background-color:red">' . __('Napló nem található', 'mav-it') . '</h4>';
        }

        foreach ($package_log as $entry) {
            $status =
                $row .= '<div align="left" class="row">';
            $row .= date('Y-m-d H:i', $entry['timestamp']) . '<br>';
            $row .= $this->displayStatus($entry['status'], $entry['status_text']);
            $row .= '</div>';
            // unset($row);
            $row .= '</div>';
        }
        echo $row;

        wp_die();
    }

    public function displayStatus($status, $status_text)
    {
        return $status_text != "" ? $status_text : $status;
    }

    public function package_details()
    {
        $groupId = Deliveo_Request_Filter::getInstance()
            ->filterBasicCode(INPUT_POST, 'deliveo_id');

        $settings        = $this->deliveo_settings;
        $package_log_url = "https://api.deliveo.eu/package/" . $groupId . "?licence=" . $settings["licence_key"] . "&api_key=" . $settings["api_key"];
        // $signature_url = "https://api.deliveo.eu/signature/" . $groupId . "?licence=" . $settings["licence_key"] . "&api_key=" . $settings["api_key"];

        $package_details = json_decode(wp_remote_fopen($package_log_url), true)['data'];
        if ($package_details[0]['dropped_off'] != null) {
            $delivered = __('Átvette', 'mav-it') . ' (' . $package_details[0]['dropped_off'] . ')';
        } else {
            $delivered = __('A küldemény még nincs kézbesítve.', 'mav-it');
        }
        if (isset($package_details[0])) {

            echo '<div class="content">' . $delivered . '<br></div>';
            echo '<div class="group_info" style="background-image:url(' . MAV_IT_DELIVEO_DIR_URL . 'images/deliveo-icon.png' . ')">';

            echo '<h4>' . __('Címzett', 'mav-it') . '</h4>';
            echo '<div class="content">[' . $package_details[0]['consignee_zip'] . '] ' . $package_details[0]['consignee_city'] . '</div>';
            echo '<div class="content">' . $package_details[0]['consignee_address'] . ' ' . $package_details[0]['consignee_apartment'] . '</div>';
            echo '<div class="content">' . $package_details[0]['consignee_phone'] . ' | <a href="mailto:' . $package_details[0]['consignee_email'] . '">' . $package_details[0]['consignee_email'] . '</a></div>';

            echo '</div>';
            echo '</div>';
        } else {
            echo '<div class="group_info" style="background-image:url(' . MAV_IT_DELIVEO_DIR_URL . 'images/deliveo-icon.png' . ')">';
            echo '<h4>' . __('A küldemény nem található.', 'mav-it') . '</h4>';
            echo '</div>';
        }
        wp_die();
    }
}
