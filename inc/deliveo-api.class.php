<?php

class Deliveo_API
{
    public $api_settings_obj, $api_settings, $licence, $api_key, $api_url, $api_package_post_url, $api_shipping_options_url, $result_message, $admin_orders_url;
    public function __construct($api_settings_obj)
    {
        $this->api_settings_obj = $api_settings_obj;
        $this->api_settings     = $this->api_settings_obj->get_deliveo_settings();

        $this->licence = $this->api_settings['licence_key'];
        $this->api_key = $this->api_settings['api_key'];
        $this->api_url = 'https://api.deliveo.eu/[TYPE]?licence=[LICENCE]&api_key=[API_KEY]';

        $this->api_package_post_url     = $this->set_api_url('package/create');
        $this->api_shipping_options_url = $this->set_api_url('delivery');
        $this->result_message           = '';
        $this->admin_orders_url         = get_bloginfo('url') . '/wp-admin/admin.php?page=wc-orders';
    }

    public function send_order_items($order_id, $order, $export_allowed)
    {
        $response = wp_remote_post($this->api_package_post_url, array(
            'method'      => 'POST',
            'timeout'     => 45,
            'redirection' => 5,
            'httpversion' => '1.0',
            'blocking'    => true,
            'headers'     => array(),
            'body'        => $order,
            'cookies'     => array(),
            'headers'     => array(
                'Source' => 'WooCommerce',
            ),
        ));
        $resp     = json_decode($response['body']);
        if (isset($resp->data[0])) {
            update_post_meta($order_id, '_deliveo_exported', 'true');
            update_post_meta($order_id, '_group_code', $resp->data[0]);

            $this->api_settings_obj->set_delivered_order_status($order_id);
        } else {
            $order = wc_get_order($order_id);
            $order->add_order_note(
                'Deliveo: ' . $resp->message . " " . (($resp->item ?? $resp->field) ?? '')
            );
        }




        return $resp;
    }

    /** Get Shipping options by DELIVEO API */
    public function get_shipping_options()
    {
        $shipping_options = false;

        $result = json_decode(wp_remote_fopen($this->api_shipping_options_url));

        if (isset($result->data) && $result->type == 'success') {
            $shipping_options = $result->data;
        }
        return $shipping_options;
    }

    private function set_api_url($type)
    {
        $api_url = $this->api_url;

        return str_replace(array('[TYPE]', '[LICENCE]', '[API_KEY]'), array($type, $this->licence, $this->api_key), $api_url);
    }
}
