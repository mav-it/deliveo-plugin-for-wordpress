<?php

class MAV_IT_Deliveo_Filter
{

    public function __construct()
    {
        // $this->deliveo_settings_obj = new MAV_IT_Deliveo_Settings();
        add_action('wp_login', array($this, 'updateShippingOptions'));

        add_action('restrict_manage_posts', array($this, 'not_exported_products_filter'));
        add_action('pre_get_posts', array($this, 'apply_not_exported_products_filter'));
        add_filter('manage_edit-shop_order_columns', array($this, 'add_deliveo_columns_header'), 20);
        add_action('manage_shop_order_posts_custom_column', array($this, 'add_order_group_code_column_content'));
        add_action('manage_shop_order_posts_custom_column', array($this, 'add_order_packaging_column_content'));
        add_action('admin_footer-edit.php', array($this, 'custom_bulk_admin_footer'));
        add_action('load-edit.php', array($this, 'custom_bulk_action'));

        add_filter('post_class', function ($classes) {
            $classes[] = 'no-link';
            return $classes;
        });
    }

    public function updateShippingOptions()
    {
        $deliveo_api = new Deliveo_API(new MAV_IT_Deliveo_Settings());
        $options = new MAV_IT_Deliveo_Settings();
        $shipping_options = $deliveo_api->get_shipping_options();

        $opts = [];
        if ($shipping_options) {
            foreach ($shipping_options as $opt) {
                $opts[] = [
                    'value' => $opt->value,
                    'description' => $opt->description,
                    'alias' => $opt->alias,
                    'shipping_default' => $opt->shipping_default,
                ];
            }
            $options->deliveo_settings['shipping_options'] = $opts;

            $settings = $options;
            update_option('mav_it_deliveo_settings', json_encode($settings->deliveo_settings));
        }
    }

    public function add_deliveo_columns_header($columns)
    {
        $new_columns = array();
        $index = 0;

        foreach ($columns as $column_name => $column_info) {
            $new_columns[$column_name] = $column_info;

            if ($index == 1) {
                $new_columns['group_code'] = '<img style="vertical-align:middle" src="' . MAV_IT_DELIVEO_DIR_URL . 'images/deliveo-icon.png" alt="DeliveoID" title="' . __('Amelyik rendelésnél ilyen ikont lát, az már fel lett adva Deliveo API-n keresztül.', 'mav-it') . '" /> DeliveoID';

                if (json_decode(get_option('mav_it_deliveo_settings'))->packaging_unit == 2) {
                    $new_columns['packaging_unit'] = 'Colli';
                }
            }

            $index += 1;
        }

        return $new_columns;
    }

    public function add_order_group_code_column_content($column)
    {
        global $post;
        if ($column == 'group_code') {
            $exported = get_metadata('post', $post->ID, '_deliveo_exported', true);
            $group_code = get_metadata('post', $post->ID, '_group_code', true);

            if ($exported == 'true') {
                echo '<span class="deliveo-cell" style="cursor:pointer;font-weight:600;color:#0073aa;" data-groupid="' . $group_code . '"> ' . $group_code . '</span>';
            } else {
                $deliveo_api = new Deliveo_API(new MAV_IT_Deliveo_Settings());
                $selected = '';
                $opts = json_decode(get_option('mav_it_deliveo_settings'))->shipping_options;
                echo "<input type='hidden' name='order[" . $post->ID . "][id]' value='" . $post->ID . "'>";
                echo '<select data-id="' . $post->ID . '" style="max-width:100%" name="order[' . $post->ID . '][option]" title="' . __('Válasszon szállítási módot', 'mav-it') . '">';
                foreach ($opts as $option) {
                    if ($deliveo_api->api_settings_obj->deliveo_settings["delivery"] == $option->value) {
                        $selected = 'selected';
                    } else {
                        $selected = '';
                    }
                    echo '<option  value="' . $option->value . '" ' . $selected . '>' . $option->description . '</option>';
                }
                echo '</select>';
            }
        }
    }

    // Csolli oszlop hozzáadása
    public function add_order_packaging_column_content($column)
    {
        global $post;

        $order = new WC_Order($post->ID);

        if ($column == 'packaging_unit') {
            $exported = get_metadata('post', $post->ID, '_deliveo_exported', true);

            $packaking_unit = json_decode(get_option('mav_it_deliveo_settings'))->packaging_unit;
            if ($exported != 'true') {
                switch ((int)$packaking_unit) {
                    case 0:
                        echo "<input title='Colli' name='order[" . $post->ID . "][unit]' type='number' readonly value='1'>";
                        break;
                    case 1:
                        echo "<input title='Colli' name='order[" . $post->ID . "][unit]' type='number' readonly value='" . $order->get_item_count() . "'>";
                        break;
                    case 2:
                        echo "<input title='Colli' name='order[" . $post->ID . "][unit]' type='number' value='" . $order->get_item_count() . "' max='" . $order->get_item_count() . "' min='1'>";
                        break;
                    default:
                        echo "<input title='Colli' type='number' value='" . $order->get_item_count() . "' max='" . $order->get_item_count() . "' min='1'>";
                        break;
                }
            }
        }
    }

    /* Order filter functions */
    public function not_exported_products_filter($post_type)
    {
        $post_type = Deliveo_Request_Filter::getInstance()
            ->getPostType(INPUT_GET, 'post_type');

        $selected_1 = '';
        $selected_2 = '';

        $deliveoExported = Deliveo_Request_Filter::getInstance()
            ->getDeliveoExported(INPUT_GET, 'deliveo_exported');
        if ($deliveoExported !== null) {
            switch ($deliveoExported) {
                case 'not_exported':
                    $selected_1 = 'selected';
                    break;
                case 'exported':
                    $selected_2 = 'selected';
                    break;
            }
        }

        if ($post_type == 'shop_order') {
            echo '
			<select name="deliveo_exported">
				<option value="">' . __('Deliveo szűrés: mind', 'mav-it') . '</option>
				<option value="not_exported" ' . $selected_1 . '>' . __('Feladatlan', 'mav-it') . '</option>
				<option value="exported" ' . $selected_2 . '>' . __('Feladott', 'mav-it') . '</option>
			</select>';
        }
    }

    /* In the Orders admin page when the deliveo filter was applied, this query filter will working  */
    public function apply_not_exported_products_filter($query)
    {
        global $pagenow;
        $meta_key_query = array();

        $postType = Deliveo_Request_Filter::getInstance()
            ->getPostType(INPUT_GET, 'post_type');
        $deliveoExported = Deliveo_Request_Filter::getInstance()
            ->getDeliveoExported(INPUT_GET, 'deliveo_exported');

        if ($query->is_admin && $pagenow == 'edit.php' && $deliveoExported !== null && $postType == 'shop_order') {
            switch ($deliveoExported) {
                case 'not_exported':
                    $query_filters = array(
                        'key' => '_deliveo_exported',
                        'compare' => 'NOT EXISTS',
                    );
                    break;

                case 'exported':
                    $query_filters = array(
                        'key' => '_deliveo_exported',
                        'value' => 'true',
                    );
                    break;
            }

            $meta_key_query = array($query_filters);

            if (count($meta_key_query > 0)) {
                $query->set('meta_query', $meta_key_query);
            }
        }
    }

    public function custom_bulk_admin_footer()
    {

        global $post_type;

        $postType = Deliveo_Request_Filter::getInstance()
            ->getPostType(INPUT_GET, 'post_type');

        if ($postType == 'shop_order' || $_GET['post_type'] == "shop_order") {
?>
            <script type="text/javascript">
                jQuery(document).ready(function() {
                    jQuery('select[name="action"]').append("<option style='font-weight:bold;' value='deliveo_send'><?php echo __('Deliveo API feladás', 'mav-it') ?></option>");
                    jQuery('select[name="action"]').append("<option style='font-weight:bold;' value='deliveo_export'><?php echo __('Deliveo API export', 'mav-it') ?></option>");
                });
            </script>
<?php
        }
    }

    function custom_bulk_action()
    {

        $wp_list_table = _get_list_table('WP_Posts_List_Table');
        $action = $wp_list_table->current_action();


        switch ($action) {
            case 'deliveo_send':

                $deliveo = new MAV_IT_Deliveo(new MAV_IT_Deliveo_Settings());

                $succeded = 0;
                $failed = 0;
                $failed_message = [];

                $orderArray = Deliveo_Request_Filter::getInstance()
                    ->getOrderArray(INPUT_GET, 'order');
                $postArray = Deliveo_Request_Filter::getInstance()
                    ->getIntegerArray(INPUT_GET, 'post');

                foreach ($orderArray as $order) {
                    if (in_array($order['id'], $postArray)) {
                        $send = $deliveo->send_by_api((int)$order['id'], $order['option'], $order['unit']);

                        if ($send->type == "success") {
                            $succeded++;
                        }
                        if ($send->type == "warning") {
                            $failed++;
                            $failed_message[] = 'OrderID: ' . $order['id'] . ' - ' . $send->msg . ": " . ($send->field ?? '');
                        }
                    }
                }
                $feedback = array(
                    'deliveo_ok' => true,
                    'succeded' => $succeded,
                    'failed' => $failed,
                    'failed_messages' => $failed_message
                );

                $sendback = add_query_arg($feedback, wp_get_referer());

                break;
            case 'deliveo_export':
                $deliveo = new MAV_IT_Deliveo(new MAV_IT_Deliveo_Settings());
                $ids = [];
                $orderArray = Deliveo_Request_Filter::getInstance()
                    ->getOrderArray(INPUT_GET, 'order');
                if (count($orderArray) > 0) {

                    foreach ($orderArray as $order) {
                        $ids[] = $order['id'];
                    }
                    $deliveo->generate_csv($ids);
                } else {
                }
                $sendback = add_query_arg(array(
                    'deliveo_export_failed' => true
                ), wp_get_referer());
                // $sendback = add_query_arg(array('deliveo_ok' => true, 'succeded' => $succeded, 'failed' => $failed), wp_get_referer());

                break;
            default:
                return;
        }

        wp_redirect($sendback);

        exit();
    }
}
