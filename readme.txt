=== Deliveo plugin for WordPress ===

Contributors: mavit
Plugin Name: Deliveo
Description: Deliveo plugin for WordPress
Developer: Deliveo
Developer URI: https://deliveo.eu
Text Domain: deliveo
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Tested up to: 6.7
Requires PHP: 8.2
Stable tag: 2.4.8
Tags: courier, Mav-IT, delivery, shipment

== Description ==

This plugin allows You to transfer WooCommerce orders into Deliveo Courier System via API. For using this plugin, You have to get a license Deliveo Courier Software here: https://deliveo.eu

== Installation ==

1. Log into WordPress admin, navigate to *Plugins*, and click *Add New*.
2. Search: Deliveo
3. Click *Install*.
4. Done. :)

= Translations =

* Bulgarian
* Czech
* Danish
* English
* Estonian
* Finnish
* French
* German
* Greek
* Hungarian
* Italian
* Latvian
* Lithuanian
* Norwegian
* Polish
* Portuguese
* Romanian
* Slovak
* Slovenian
* Spanish
* Swedish
* Turkish

= Recommended Plugins =

* WooCommerce
